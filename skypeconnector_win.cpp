#include "skypeconnector_win.h"
#include <tlhelp32.h>
#include <QTimer>
#include <QDebug>

CSkypeProxyWidget::CSkypeProxyWidget(QWidget *parent)
    : QWidget(parent)
{
    setWindowFlags(Qt::Tool | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);

    QPalette palette;
    palette.setBrush(backgroundRole(), Qt::transparent);
    setPalette(palette);

    isSkypeAvailable_ = false;
    skypeWindowHandle_ = (HWND)INVALID_HANDLE_VALUE;
    skypeApiDiscover_ = RegisterWindowMessage(L"SkypeControlAPIDiscover");
    skypeApiAttach_ = RegisterWindowMessage(L"SkypeControlAPIAttach");
}

bool CSkypeProxyWidget::winEvent(MSG *message, long *result)
{
    if (message->message == skypeApiAttach_)
    {
        if (message->lParam == 0)
        {
            skypeWindowHandle_ = (HWND)message->wParam;
            emit attached();
        }
        else if (message->lParam == 2)
        {
            emit attachFailed(tr("Access denied"));
        }
        else if (message->lParam == 3)
        {
            emit attachFailed(tr("Skype is not available (maybe you're not logged in"));
        }
        else if (message->lParam == 0x8001)
        {
            isSkypeAvailable_ = true;
            emit skypeAvailable();
        }
    }
    else if (message->message == WM_COPYDATA && (HWND)message->wParam == skypeWindowHandle_)
    {
        // Get the passed data
        COPYDATASTRUCT cds;
        memcpy(&cds, (const void*)message->lParam, sizeof(COPYDATASTRUCT));
        QString mes((const char*)cds.lpData);

        emit notificationReceived(mes);

        *result = 1;
        return true;
    }
    return false;

}

CSkypeConnector_Win::CSkypeConnector_Win()
{
    availableTimer_ = new QTimer(this);
    availableTimer_->setInterval(5000);
    connect(availableTimer_, SIGNAL(timeout()), SLOT(onAvailabilityTimer()));
    availableTimer_->start();

    proxyWidget_ = new CSkypeProxyWidget;

    connect(proxyWidget_, SIGNAL(attached()), SIGNAL(attached()));
    connect(proxyWidget_, SIGNAL(attachFailed(QString)), SIGNAL(attachFailed(QString)));
    connect(proxyWidget_, SIGNAL(notificationReceived(QString)), SIGNAL(notificationReceived(QString)));
    connect(proxyWidget_, SIGNAL(skypeAvailable()), SIGNAL(skypeAvailable()));

    proxyWidget_->setFixedSize(3, 3);
    proxyWidget_->show();
    QTimer::singleShot(100, proxyWidget_, SLOT(hide()));
}

CSkypeConnector_Win::~CSkypeConnector_Win()
{
    if (proxyWidget_)
        delete proxyWidget_;
}

bool CSkypeConnector_Win::isSkypeRunning() const
{
#ifdef Q_WS_WIN
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    if (Process32First(snapshot, &entry) == TRUE)
    {
       while (Process32Next(snapshot, &entry) == TRUE)
       {
           if (QString::fromWCharArray(entry.szExeFile).toLower() == "skype.exe")
           {
               return true;
           }
       }
    }
    CloseHandle(snapshot);
#endif
    return false;
}

void CSkypeConnector_Win::attach()
{
    DWORD res;
    LRESULT r = SendMessageTimeout((HWND)HWND_BROADCAST, (UINT)proxyWidget_->skypeApiDiscover_,
         (WPARAM)proxyWidget_->effectiveWinId(), (LPARAM)0, 0, 100, &res);
}

void CSkypeConnector_Win::sendMessage(const QString &command)
{
    int dataLength = command.size() + 1;
    char *data = new char[dataLength];
    memset(data, 0, dataLength * sizeof(char));
    strcpy(data, command.toStdString().c_str());

    PCOPYDATASTRUCT pCds = new COPYDATASTRUCT;
    pCds->dwData = 1;
    pCds->lpData = data;
    pCds->cbData = sizeof(*pCds) + dataLength;

    DWORD result;
    LRESULT returnValue = SendMessageTimeout((HWND)proxyWidget_->skypeWindowHandle_,
        WM_COPYDATA, (WPARAM)proxyWidget_->effectiveWinId(), (LPARAM)pCds,
        SMTO_NORMAL, 100, &result);

    qDebug() << QString("CSkypeConnector_Win::sendSkypeCommand(\"%1\"). SendMessageTimeout. return value = %2, result = %3")
                    .arg(command)
                    .arg(returnValue)
                    .arg(result);

    delete data;
    delete pCds;
}

void CSkypeConnector_Win::onAvailabilityTimer()
{
    if (!isSkypeRunning() && proxyWidget_->isSkypeAvailable_)
    {
        proxyWidget_->isSkypeAvailable_ = false;
        emit skypeUnavailable();
    }
}
