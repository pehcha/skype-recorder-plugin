#ifndef CSKYPEMANAGER_H
#define CSKYPEMANAGER_H

#include <QObject>
#include "callinfo.h"
#include "iskypeconnector.h"

class ISkypeConnector;
class CSkypeManager : public QObject
{
    Q_OBJECT

public:
    CSkypeManager(QObject *parent=0);

public slots:
    void onNotificationReceived(const QString &message);
    void onSkypeAvailable();

signals:
    void callStarted();
    void savingRecordsStarted();
    void recordsSaved(const QString &absPath);

private:
    ISkypeConnector *skypeConnector_;
    CCallInfoList calls_;

    void getCallInterlocutorName(int callId);
    void redirectCallSound(const CCallInfo &ci);
};

#endif // CSKYPEMANAGER_H
