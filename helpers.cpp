#include "helpers.h"
#include <sndfile.h>
#include <lame/lame.h>
#include <QDebug>

#ifdef Q_WS_MAC
QString fromCFtoQString(CFStringRef str)
{
    if (!str)
        return QString();

    CFIndex length = CFStringGetLength(str);
    if (length == 0)
        return QString();

    QString string(length, Qt::Uninitialized);
    CFStringGetCharacters(str, CFRangeMake(0, length), reinterpret_cast<UniChar *>
                          (const_cast<QChar *>(string.unicode())));
    return string;
}
#endif

#define BUFFER_LEN 1024
#define MAX_CHANNELS 6

static void data_processing(double *data, int count, int channels)
{
    double channel_gain [MAX_CHANNELS] = { 0.5, 0.8, 0.1, 0.4, 0.4, 0.9 } ;
    int k, chan ;

    for (chan = 0 ; chan < channels ; chan ++)
        for (k = chan ; k < count ; k+= channels)
            data [k] *= channel_gain [chan] ;
    return ;
}

void mergeWavFiles(const QString &firstWavFile, const QString &secondWavFile,
     const QString &outputWavFile)
{
    static double data [BUFFER_LEN] ;
    static double data2 [BUFFER_LEN] ;
    static double outdata [BUFFER_LEN] ;

    SNDFILE *infile, *outfile, *infile2 ;
    SF_INFO sfinfo ;
    int readcount ;
    SF_INFO sfinfo2 ;
    int readcount2 ;

    if (!(infile = sf_open (firstWavFile.toStdString().c_str(), SFM_READ, &sfinfo)))
    {
        qDebug() << "Cannot open input file " << firstWavFile;
        qDebug() << sf_strerror(NULL);
        return;
    }

    if (sfinfo.channels > MAX_CHANNELS)
    {
        qDebug() << QString("File info 1: Not able to process more than %1 channels").arg(MAX_CHANNELS);
        return;
    }

    if (!(infile2 = sf_open (secondWavFile.toStdString().c_str(), SFM_READ, &sfinfo2)))
    {
        qDebug() << "Cannot open input file " << secondWavFile;
        qDebug() << sf_strerror(NULL);
        return;
    }

    if (sfinfo2.channels > MAX_CHANNELS) {
        qDebug() << QString("File info 2: Not able to process more than %1 channels")
                        .arg( MAX_CHANNELS);
        return;
    }

    // Open the output file
    if (! (outfile = sf_open (outputWavFile.toStdString().c_str(), SFM_WRITE, &sfinfo)))
    {
        qDebug() << "Cannot open output file " << outputWavFile;
        qDebug() << sf_strerror (NULL);
        return;
    }

    while ((readcount = sf_read_double (infile, data, BUFFER_LEN)) &&
           (readcount2 = sf_read_double (infile2, data2, BUFFER_LEN))) {
        data_processing (data, readcount, sfinfo.channels) ;
        data_processing(data2, readcount2, sfinfo2.channels) ;

        for(int i=0; i < 1024;++i) {
            outdata[i] = (data[i] + data2[i]) -(data[i])*(data2[i])/65535;
        }

        sf_write_double (outfile, outdata , readcount) ;
    } ;

    // Close input and output files
    sf_close (infile) ;
    sf_close (infile2) ;
    sf_close (outfile) ;
}

void convertWavToMp3(const QString &wavFileAbsPath, const QString &mp3FileAbsPath)
{
    int read, write;

    FILE *pcm = fopen(wavFileAbsPath.toStdString().c_str(), "rb");
    if (!pcm)
    {
        qDebug() << "Cannot open input file " << wavFileAbsPath;
        return;
    }
    FILE *mp3 = fopen(mp3FileAbsPath.toStdString().c_str(), "wb");
    if (!mp3)
    {
        qDebug() << "Cannot open output file " << mp3FileAbsPath;
        return;
    }

    const int PCM_SIZE = 8192;
    const int MP3_SIZE = 8192;

    short int pcm_buffer[PCM_SIZE*2];
    unsigned char mp3_buffer[MP3_SIZE];

    lame_t lame = lame_init();
    lame_set_in_samplerate(lame, 16000);
    lame_set_num_channels(lame, 1);
    lame_set_quality(lame, 2);
    lame_set_mode(lame, MONO);
    lame_set_VBR(lame, vbr_default);
    lame_set_VBR_q(lame, 2);
    lame_init_params(lame);

    do {
        read = fread(pcm_buffer, sizeof(short int), PCM_SIZE, pcm);
        if (read == 0)
            write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
        else
            write = lame_encode_buffer(lame, pcm_buffer, 0, read, mp3_buffer, MP3_SIZE);
        fwrite(mp3_buffer, write, 1, mp3);
    } while (read != 0);

    lame_close(lame);
    fclose(mp3);
    fclose(pcm);
}
