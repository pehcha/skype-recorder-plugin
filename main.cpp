#include <QApplication>
#include "applicationmanager.h"
#include "settings.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    CApplicationManager am;
    a.setOrganizationName(CSettings::instance()->organizationName());
    a.setApplicationName(CSettings::instance()->applicationName());
    a.setQuitOnLastWindowClosed(false);
    
    return a.exec();
}
