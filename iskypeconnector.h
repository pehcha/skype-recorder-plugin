#ifndef SKYPECONNECTOR_H
#define SKYPECONNECTOR_H

#include <QObject>

#define SKYPE_PLUGIN_NAME "TopTal Audio Recorder"

class ISkypeConnector : public QObject
{
    Q_OBJECT
public:
    static ISkypeConnector* instance();
    virtual ~ISkypeConnector() { /* VOID */ };

    virtual bool isSkypeRunning() const = 0;
    virtual bool isAttached() const;

public slots:
    virtual void attach() = 0;
    virtual void sendMessage(const QString &message) = 0;

signals:
    void skypeAvailable();
    void skypeUnavailable();
    void attached();
    void attachFailed(const QString &errorDesc);
    void notificationReceived(const QString &notification);

protected:
    bool isAttached_;

    ISkypeConnector();

private slots:
    void onAttached();
    void onSkypeUnavailable();
};

#endif // SKYPECONNECTOR_H
