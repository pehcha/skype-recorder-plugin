#include "iskypeconnector.h"
#include "skypeconnector_mac.h"
#include "skypeconnector_win.h"

ISkypeConnector* ISkypeConnector::instance()
{
#ifdef Q_WS_WIN
    static CSkypeConnector_Win inst;
#endif

#ifdef Q_WS_MAC
    static CSkypeConnector_Mac inst;
#endif

    return &inst;
}

ISkypeConnector::ISkypeConnector()
    : isAttached_(false)
{
    QObject::connect(this, SIGNAL(attached()), SLOT(onAttached()));
    QObject::connect(this, SIGNAL(skypeUnavailable()), SLOT(onSkypeUnavailable()));
}

void ISkypeConnector::onAttached()
{
    isAttached_ = true;
}

void ISkypeConnector::onSkypeUnavailable()
{
    isAttached_ = false;
}

bool ISkypeConnector::isAttached() const
{
    return isAttached_;
}
