#ifndef SKYPECONNECTOR_MAC_H
#define SKYPECONNECTOR_MAC_H

#include "iskypeconnector.h"
#ifdef Q_WS_MAC
class CSkypeConnector_Mac : public ISkypeConnector
{
    friend class ISkypeConnector;
    Q_OBJECT
    
public:
    ~CSkypeConnector_Mac();

    bool isSkypeRunning() const;

public slots:
    void attach();
    void sendMessage(const QString &message);

protected:
    CSkypeConnector_Mac();
};
#endif

#endif // SKYPECONNECTOR_MAC_H
