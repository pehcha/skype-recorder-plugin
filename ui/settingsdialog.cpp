#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QFileDialog>

CSettingsDialog::CSettingsDialog(QWidget *parent)
    : QDialog(parent),
      ui_(new Ui::dlgSettings)
{
    ui_->setupUi(this);
    connect(ui_->pbBrowseRecordLocation, SIGNAL(clicked()), SLOT(onBrowseRecordLocation()));
    connect(ui_->leRecordLocation, SIGNAL(textChanged(QString)), SLOT(onRecordLocationChanged(QString)));
}

CSettingsDialog::~CSettingsDialog()
{
    delete ui_;
}

QString CSettingsDialog::recordLocation() const
{
    return ui_->leRecordLocation->text();
}

void CSettingsDialog::setRecordLocation(const QString &location)
{
    ui_->leRecordLocation->setText(location);
}

void CSettingsDialog::setSettings(const SSettings &s)
{
    ui_->leRecordLocation->setText(s.recordLocation);
}

SSettings CSettingsDialog::settings() const
{
    SSettings s;
    s.recordLocation = ui_->leRecordLocation->text();

    return s;
}

void CSettingsDialog::onBrowseRecordLocation()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
        QString(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir.isEmpty())
        return;

    ui_->leRecordLocation->setText(dir);
}

void CSettingsDialog::onRecordLocationChanged(const QString &location)
{
    ui_->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(QFile::exists(location));
}
