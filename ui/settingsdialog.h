#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "settings.h"

namespace Ui
{
    class dlgSettings;
}

class CSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    CSettingsDialog(QWidget *parent = 0);
    ~CSettingsDialog();

    void setSettings(const SSettings &s);
    SSettings settings() const;

    QString recordLocation() const;
    void setRecordLocation(const QString &location);

private:
    Ui::dlgSettings *ui_;

private slots:
    void onBrowseRecordLocation();
    void onRecordLocationChanged(const QString &location);
};

#endif
