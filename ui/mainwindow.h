#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class CMainWindow;
}

class QAction;
class QSystemTrayIcon;
class CMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    enum State
    {
        kSkypeUnavailable = 0,
        kSkypeAvailable,
        kAttaching,
        kAttached,
        kRecording,
        kSavingRecords
    };

    explicit CMainWindow(QWidget *parent = 0);
    ~CMainWindow();

    void setState(State state);

public slots:
    void onCallStarted();
    void onSkypeAvailable();
    void onSkypeUnavailable();
    void onStartAttaching();
    void onAttached();
    void onAttachFailed(const QString& errorDesc);
    void onStartSavingRecords();
    void onRecordsSaved(const QString &absPath);

private:
    Ui::CMainWindow *ui_;
    QAction *actionExit_;
    QAction *actionSettings_;
    QMenu *trayMenu_;
    QSystemTrayIcon *trayIcon_;
    State state_;

    void createTrayIcon();
    QString stateAsString(State state);

private slots:
    void onSettings();
};

#endif // MAINWINDOW_H
