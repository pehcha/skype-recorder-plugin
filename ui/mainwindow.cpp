#include "mainwindow.h"
#include "settingsdialog.h"
#include "ui_mainwindow.h"
#include <QAction>
#include <QApplication>
#include <QMenu>
#include <QSystemTrayIcon>

CMainWindow::CMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::CMainWindow),
    state_(kSkypeUnavailable)
{
    ui_->setupUi(this);
    setWindowIcon(QIcon(":/res/icon.png"));
    createTrayIcon();
}

CMainWindow::~CMainWindow()
{
    delete ui_;
}

void CMainWindow::createTrayIcon()
{
    actionExit_ = new QAction(tr("Quit"), this);
    actionSettings_ = new QAction(tr("Settings..."), this);

    connect(actionExit_, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(actionSettings_, SIGNAL(triggered()), SLOT(onSettings()));

    trayMenu_ = new QMenu(this);
    trayMenu_->addAction(actionSettings_);
    trayMenu_->addAction(actionExit_);

    trayIcon_ = new QSystemTrayIcon(this);
    trayIcon_->setContextMenu(trayMenu_);

    trayIcon_->setIcon(windowIcon());
    trayIcon_->show();
}

void CMainWindow::onSettings()
{
    CSettingsDialog *dlg = new CSettingsDialog;
    dlg->setSettings(CSettings::instance()->settings());
    if (dlg->exec() == QDialog::Accepted)
    {
        CSettings::instance()->setSettings(dlg->settings());
    }
    delete dlg;
}

QString CMainWindow::stateAsString(State state)
{
    switch (state)
    {
    case kSkypeUnavailable : return tr("Not attached. Skype is unavailable");
    case kSkypeAvailable : return tr("Not attached. Skype is available");
    case kAttaching : return tr("Connecting to Skype...");
    case kAttached: return tr("Attached to Skype");
    case kRecording : return tr("Recording...");
    case kSavingRecords : return tr("Saving records...");
    }
}

void CMainWindow::setState(State state)
{
    state_ = state;
    trayIcon_->setToolTip(QString("%1\n%2")
        .arg(CSettings::instance()->applicationName())
        .arg(stateAsString(state)));
}

void CMainWindow::onSkypeAvailable()
{
    setState(kSkypeAvailable);
    trayIcon_->showMessage(CSettings::instance()->applicationName(),
        tr("Skype became available"),
        QSystemTrayIcon::Information, 1500);
}

void CMainWindow::onSkypeUnavailable()
{
    setState(kSkypeUnavailable);
    trayIcon_->showMessage(CSettings::instance()->applicationName(),
        tr("Skype became unavailable"),
        QSystemTrayIcon::Information, 1500);
}

void CMainWindow::onStartAttaching()
{
    setState(kAttaching);
}

void CMainWindow::onAttached()
{
    setState(kAttached);
    trayIcon_->showMessage(CSettings::instance()->applicationName(),
        tr("Attached to Skype"),
        QSystemTrayIcon::Information, 1500);
}

void CMainWindow::onCallStarted()
{
    setState(kRecording);
    trayIcon_->showMessage(CSettings::instance()->applicationName(),
        tr("New call started. Recording..."),
        QSystemTrayIcon::Information, 1500);
}

void CMainWindow::onAttachFailed(const QString& errorDesc)
{
    setState(kSkypeAvailable);
    trayIcon_->showMessage(CSettings::instance()->applicationName(),
        tr("Couldn't attach to Skype. Error: %1").arg(errorDesc),
        QSystemTrayIcon::Critical, 5000);
}

void CMainWindow::onStartSavingRecords()
{
    trayIcon_->showMessage(CSettings::instance()->applicationName(),
        tr("Saving records..."), QSystemTrayIcon::Information, 1000);
}

void CMainWindow::onRecordsSaved(const QString &absPath)
{
    trayIcon_->showMessage(CSettings::instance()->applicationName(),
        tr("The records saved to %1").arg(absPath),
        QSystemTrayIcon::Information, 2500);
}
