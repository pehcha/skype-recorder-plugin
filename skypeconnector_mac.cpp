#include "skypeconnector_mac.h"

#ifdef Q_WS_MAC
#include "Skype.h"
#include "helpers.h"
#include <QDebug>

//---------- Skype callbacks
void skypeNotificationReceived(CFStringRef aNotificationString)
{
    QString message = fromCFtoQString(aNotificationString);
    QMetaObject::invokeMethod(ISkypeConnector::instance(),
        "notificationReceived",
        Q_ARG(const QString &, message));
}

void skypeAttachResponse(unsigned int aAttachResponseCode)
{
    if (aAttachResponseCode == 0)
    {
        QMetaObject::invokeMethod(ISkypeConnector::instance(),
            "attachFailed",
            Q_ARG(const QString&, QObject::tr("An error occured while connecting to Skype")));
    }
    else if (aAttachResponseCode == 1)
    {
        QMetaObject::invokeMethod(ISkypeConnector::instance(), "attached");
    }
}

void skypeBecameAvailable(CFPropertyListRef aNotification)
{
    Q_UNUSED(aNotification);
    QMetaObject::invokeMethod(ISkypeConnector::instance(), "skypeAvailable");
}

void skypeBecameUnavailable(CFPropertyListRef aNotification)
{
    Q_UNUSED(aNotification);
    QMetaObject::invokeMethod(ISkypeConnector::instance(), "skypeUnavailable");
}
//---------- END. Skype callbacks

CSkypeConnector_Mac::CSkypeConnector_Mac()
{
    SkypeDelegate *delegate = new SkypeDelegate;

    delegate->SkypeNotificationReceived = skypeNotificationReceived;
    delegate->SkypeAttachResponse = skypeAttachResponse;
    delegate->SkypeBecameAvailable = skypeBecameAvailable;
    delegate->SkypeBecameUnavailable = skypeBecameUnavailable;
    delegate->clientApplicationName = CFSTR(SKYPE_PLUGIN_NAME);

    SetSkypeDelegate(delegate);
}

CSkypeConnector_Mac::~CSkypeConnector_Mac()
{
    DisconnectFromSkype();
    SkypeDelegate *delegate = GetSkypeDelegate();
    RemoveSkypeDelegate();
    if (delegate)
        delete delegate;
}

bool CSkypeConnector_Mac::isSkypeRunning() const
{
    return IsSkypeRunning();
}

void CSkypeConnector_Mac::attach()
{
    ConnectToSkype();
}

void CSkypeConnector_Mac::sendMessage(const QString &message)
{
    CFStringRef cfMessage = CFStringCreateWithCString(kCFAllocatorDefault,
        message.toStdString().c_str(),
        kCFStringEncodingMacRoman);
    SendSkypeCommand(cfMessage);
    CFRelease(cfMessage);
}
#endif
