#include "skypemanager.h"
#include "helpers.h"
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QRegExp>
#include <QDesktopServices>
#include <QDebug>

const char kRedirectInputToFileTemplate[] = "ALTER CALL %1 SET_OUTPUT FILE=\"%2\"";
const char kRedirectOutputToFileTemplate[] = "ALTER CALL %1 SET_CAPTURE_MIC FILE=\"%2\"";

const char kCallRoutingPattern[] = "CALL (\\d+) STATUS RINGING";
const char kCallPartnerHandlePattern[] = "CALL (\\d+) PARTNER_HANDLE (\\S+)";
const char kCallFinishedPattern[] = "CALL (\\d+) STATUS FINISHED";
const char kConnectionLoggedOutPattern[] = "CONNSTATUS LOGGEDOUT";

const QRegExp kRxpCallRouting = QRegExp(kCallRoutingPattern, Qt::CaseInsensitive);
const QRegExp kRxpPartnerHandle = QRegExp(kCallPartnerHandlePattern, Qt::CaseInsensitive);
const QRegExp kRxpCallFinished = QRegExp(kCallFinishedPattern, Qt::CaseInsensitive);

CSkypeManager::CSkypeManager(QObject *parent)
    : QObject(parent)
{
    skypeConnector_ = ISkypeConnector::instance();

    connect(skypeConnector_, SIGNAL(notificationReceived(QString)), SLOT(onNotificationReceived(QString)));
    connect(skypeConnector_, SIGNAL(skypeAvailable()), SLOT(onSkypeAvailable()));

    if (skypeConnector_->isSkypeRunning())
        skypeConnector_->attach();
}

void CSkypeManager::onNotificationReceived(const QString &message)
{
    qDebug() << "Skype:" << message;
    int pos = 0;
    if (kRxpCallRouting.indexIn(message, pos) != -1)
    {
        int callId = kRxpCallRouting.cap(1).toInt();

        qDebug() << "New call. Id:" << callId;
        emit callStarted();

        calls_.appendCallInfo(callId);
        getCallInterlocutorName(callId);
    }
    else if (kRxpPartnerHandle.indexIn(message, pos) != -1)
    {
        int callId = kRxpPartnerHandle.cap(1).toInt();
        QString partnerHandle = kRxpPartnerHandle.cap(2);

        qDebug() << QString("Call %1. Partner handle: %2").arg(callId).arg(partnerHandle);

        CCallInfo &ci = calls_.getCallInfo(callId);
        if (ci.isValid())
        {
            ci.setPartnerName(partnerHandle);
            ci.setSkypeInputWavFilePath(QString("%1/%2%3_inp_tt_sk_pl")
                .arg(QDir::tempPath())
                .arg(QDateTime::currentDateTime().toTime_t())
                .arg(qrand()));
            ci.setSkypeOutputWavFilePath(QString("%1/%2%3_out_tt_sk_pl")
                .arg(QDir::tempPath())
                .arg(QDateTime::currentDateTime().toTime_t())
                .arg(qrand()));
            ci.setResultMp3FilePath(QString("%1/%2_%3.mp3")
                .arg(QDesktopServices::storageLocation(QDesktopServices::MusicLocation))
                .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy_hh.mm.ss"))
                .arg(partnerHandle));
            redirectCallSound(ci);
        }
        else
        {
            qDebug() << "no such call info found. callId:" << callId;
        }
    }
    else if (kRxpCallFinished.indexIn(message, pos) != -1)
    {
        int callId = kRxpCallFinished.cap(1).toInt();
        CCallInfo ci = calls_.getCallInfo(callId);

        qDebug() << "call finished. callId:" << callId << ",\n" << ci;

        QString tmpFilePath = QString("%1/%2%3_merged_tt_sk_pl")
            .arg(QDir::tempPath())
            .arg(QDateTime::currentDateTime().toTime_t())
            .arg(qrand());

        emit savingRecordsStarted();
        mergeWavFiles(ci.skypeInputWavFilePath(), ci.skypeOutputWavFilePath(), tmpFilePath);
        convertWavToMp3(tmpFilePath, ci.resultMp3FilePath());
        QFile::remove(ci.skypeInputWavFilePath());
        QFile::remove(ci.skypeOutputWavFilePath());
        QFile::remove(tmpFilePath);
        calls_.removeCallInfo(callId);

        emit recordsSaved(ci.resultMp3FilePath());
    }
    else if (message == kConnectionLoggedOutPattern)
    {
        // user logged out. maybe we should fire unavailable signal here
    }
}

void CSkypeManager::onSkypeAvailable()
{
    qDebug() << "skype available";
    skypeConnector_->attach();
}

void CSkypeManager::getCallInterlocutorName(int callId)
{
    skypeConnector_->sendMessage(QString("GET CALL %1 PARTNER_HANDLE").arg(callId));
}

void CSkypeManager::redirectCallSound(const CCallInfo &ci)
{
    qDebug() << "redirectCallSound. Call info:" << ci;

    skypeConnector_->sendMessage(QString(kRedirectInputToFileTemplate)
                                    .arg(ci.callId())
                                    .arg(ci.skypeInputWavFilePath()));
    skypeConnector_->sendMessage(QString(kRedirectOutputToFileTemplate)
                                    .arg(ci.callId())
                                    .arg(ci.skypeOutputWavFilePath()));
}
