#include "applicationmanager.h"
#include "skypemanager.h"
#include "ui/mainwindow.h"
#include <QDateTime>

CApplicationManager::CApplicationManager(QObject *parent) :
    QObject(parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
    skypeManager_ =  new CSkypeManager(this);
    mainWindow_ = new CMainWindow();

    connect(ISkypeConnector::instance(), SIGNAL(attached()), mainWindow_, SLOT(onAttached()));
    connect(ISkypeConnector::instance(), SIGNAL(attachFailed(QString)), mainWindow_, SLOT(onAttachFailed(QString)));
    connect(ISkypeConnector::instance(), SIGNAL(skypeAvailable()), mainWindow_, SLOT(onSkypeAvailable()));
    connect(ISkypeConnector::instance(), SIGNAL(skypeUnavailable()), mainWindow_, SLOT(onSkypeUnavailable()));
    connect(skypeManager_, SIGNAL(callStarted()), mainWindow_, SLOT(onCallStarted()));
    connect(skypeManager_, SIGNAL(savingRecordsStarted()), mainWindow_, SLOT(onStartSavingRecords()));
    connect(skypeManager_, SIGNAL(recordsSaved(QString)), mainWindow_, SLOT(onRecordsSaved(QString)));
}

CApplicationManager::~CApplicationManager()
{
    delete mainWindow_;
}
