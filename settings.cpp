#include "settings.h"
#include <QSettings>
#include <QDesktopServices>

// Setting names
const char kRecordLocationSettingName[] = "record_location";

CSettings::CSettings()
{
    readSettings();
}

CSettings::~CSettings()
{
    saveSettings();
}

QString CSettings::organizationName() const
{
    return "TopTal";
}

QString CSettings::applicationName() const
{
    return "Skype Recorder Plugin";
}

CSettings* CSettings::instance()
{
    static CSettings inst;
    return &inst;
}

void CSettings::setSettings(const SSettings& s)
{
    s_ = s;
    saveSettings();
}

SSettings CSettings::settings() const
{
    return s_;
}

QString CSettings::recordLocation() const
{
    return s_.recordLocation;
}
void CSettings::setRecordLocation(const QString &path)
{
    s_.recordLocation = path;
}

void CSettings::saveSettings()
{
    // Write settings to persistent storage
    QSettings sts(organizationName(), applicationName());
    sts.setValue(kRecordLocationSettingName, s_.recordLocation);
}

void CSettings::readSettings()
{
    // Read settings from persistent storage
    QSettings sts(organizationName(), applicationName());
    s_.recordLocation = sts.value(kRecordLocationSettingName,
        QDesktopServices::storageLocation(QDesktopServices::MusicLocation)).toString();
}
