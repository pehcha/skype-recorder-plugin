#ifndef APPLICATIONMANAGER_H
#define APPLICATIONMANAGER_H

#include <QObject>

class CSkypeManager;
class CMainWindow;
class CApplicationManager : public QObject
{
    
    Q_OBJECT
public:
    explicit CApplicationManager(QObject *parent = 0);
    ~CApplicationManager();

private:
    CSkypeManager *skypeManager_;
    CMainWindow *mainWindow_;
};

#endif // APPLICATIONMANAGER_H
