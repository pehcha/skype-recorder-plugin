#include "callinfo.h"

CCallInfo CCallInfoList::invalidCallInfo = CCallInfo(0);

QDebug operator<<(QDebug qd, const CCallInfo &ci)
{
    qd.nospace() << QString("CCallInfo {\n"
                  "\tcallId: %1\n"
                  "\tpartnerName: %2\n"
                  "\tskypeInputWavFile: %3\n"
                  "\tskypeOutputWavFile: %4\n"
                  "\tresultMp3File: %5 };")
         .arg(ci.callId())
         .arg(ci.partnerName())
         .arg(ci.skypeInputWavFilePath())
         .arg(ci.skypeOutputWavFilePath())
         .arg(ci.resultMp3FilePath());
    return qd.space();
}

//------------- Call Info List
CCallInfo& CCallInfoList::getCallInfo(int callId)
{
    for (int i = 0; i < list_.size(); i++)
    {
        if (list_[i].callId() == callId)
        {
            return list_[i];
        }
    }
    return invalidCallInfo;
}

void CCallInfoList::removeCallInfo(int callId)
{
    for (QList<CCallInfo>::iterator it = list_.begin(); it != list_.end(); ++it)
    {
        if (it->callId() == callId)
        {
            list_.erase(it);
            break;
        }
    }
}
