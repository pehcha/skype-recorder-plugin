#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>

struct SSettings
{
    QString recordLocation;
};

class CSettings
{
public:
    static CSettings *instance();
    ~CSettings();

    QString organizationName() const;
    QString applicationName() const;

    void setSettings(const SSettings& s);
    SSettings settings() const;

    QString recordLocation() const;
    void setRecordLocation(const QString &path);

private:
    CSettings();
    SSettings s_;

    void saveSettings();
    void readSettings();
};

#endif // SETTINGS_H
