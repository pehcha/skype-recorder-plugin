#-------------------------------------------------
#
# Project created by QtCreator 2013-07-30T15:10:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = skype-recorder-plugin
TEMPLATE = app

SOURCES += main.cpp\
    skypeconnector_win.cpp \
    skypeconnector_mac.cpp \
    iskypeconnector.cpp \
    helpers.cpp \
    applicationmanager.cpp \
    skypemanager.cpp \
    callinfo.cpp \
    ui/settingsdialog.cpp \
    ui/mainwindow.cpp \
    settings.cpp

HEADERS += skypeconnector_win.h \
    skypeconnector_mac.h \
    iskypeconnector.h \
    helpers.h \
    applicationmanager.h \
    skypemanager.h \
    callinfo.h \
    ui/settingsdialog.h \
    ui/mainwindow.h \
    settings.h

win32 {
    INCLUDEPATH += C:/work/include
    LIBS += -LC:/work/libs -lmp3lame -lsndfile
}

macx {
    INCLUDEPATH += /usr/local/include /Users/Admin/work/Skype.framework/Headers
    QMAKE_LFLAGS += -F/Users/Admin/work
    LIBS += -L/usr/local/lib -lmp3lame -lsndfile \
        -framework Carbon \
        -framework Skype
}

FORMS += \
    ui/settingsdialog.ui

RESOURCES += \
    res.qrc
