#ifndef SKYPECONNECTOR_WIN_H
#define SKYPECONNECTOR_WIN_H

#include "iskypeconnector.h"

#ifdef Q_WS_WIN
#include <windows.h>
#include <QWidget>

class CSkypeProxyWidget : public QWidget
{
    Q_OBJECT

    friend class CSkypeConnector_Win;

public:
    CSkypeProxyWidget(QWidget *parent=0);

signals:
    void attached();
    void attachFailed(const QString &desc);
    void notificationReceived(const QString &message);
    void skypeAvailable();

protected:
    bool winEvent(MSG *message, long *result);

private:
    bool isSkypeAvailable_;
    UINT skypeApiAttach_;
    UINT skypeApiDiscover_;
    HWND skypeWindowHandle_;
};

class QTimer;
class CSkypeConnector_Win : public ISkypeConnector
{
    friend class ISkypeConnector;
    friend class CSkypeProxyWidget;
    Q_OBJECT
    
public:
    ~CSkypeConnector_Win();
    bool isSkypeRunning() const;

public slots:
    void attach();
    void sendMessage(const QString &message);

protected:
    CSkypeConnector_Win();

private:
    QTimer *availableTimer_;
    CSkypeProxyWidget *proxyWidget_;

private slots:
    void onAvailabilityTimer();
};
#endif

#endif // SKYPECONNECTOR_WIN_H
