#ifndef HELPERS_H
#define HELPERS_H

#include <QString>

#ifdef Q_WS_MAC
#include <CoreFoundation/CoreFoundation.h>
QString fromCFtoQString(CFStringRef str);
#endif

void mergeWavFiles(const QString &firstWavFile, const QString &secondWavFile,
     const QString &outputWavFile);
void convertWavToMp3(const QString &wavFileAbsPath, const QString &mp3FileAbsPath);

#endif // HELPERS_H
