#ifndef CALLINFO_H
#define CALLINFO_H

#include <QString>
#include <QDebug>

class CCallInfo
{
public:
    CCallInfo(int cId=0) : callId_(cId) { /* VOID */ }
    bool isValid() const { return callId_ > 0; }

    int callId() const { return callId_; }

    void setPartnerName(const QString &pname) { partnerName_ = pname; }
    QString partnerName() const { return partnerName_; }

    void setSkypeInputWavFilePath(const QString &absPath) { skypeInputWavFile_ = absPath; }
    QString skypeInputWavFilePath() const { return skypeInputWavFile_; }

    void setSkypeOutputWavFilePath(const QString &absPath) { skypeOutputWavFile_ = absPath; }
    QString skypeOutputWavFilePath() const { return skypeOutputWavFile_; }

    void setResultMp3FilePath(const QString &absPath) { resultMp3File_ = absPath; }
    QString resultMp3FilePath() const { return resultMp3File_; }

private:
    int callId_;
    QString partnerName_;
    QString skypeInputWavFile_;
    QString skypeOutputWavFile_;
    QString resultMp3File_;
};

class CCallInfoList
{
public:
    void appendCallInfo(int callId) { CCallInfo ci(callId); list_.append(ci); }

    CCallInfo& getCallInfo(int callId);
    void removeCallInfo(int callId);

private:
    static CCallInfo invalidCallInfo;

    QList<CCallInfo> list_;
};

QDebug operator<<(QDebug, const CCallInfo &ci);

#endif // CALLINFO_H
